public class MethodsTest {
	public static void main(String[] args) {
	//	int x = 5;
	//	methodOneInputNoReturn(x+10);
		//System.out.println(x);
	//	methodTwoInputNoReturn(5.5,5);
	//double z = sumSquareRoot(5,9);
	//System.out.println(z);
	/*String s1 = "java";
	String s2 = "programming";
	System.out.println(s1.length());
	System.out.println(s2.length());
	*/
	System.out.println(SecondClass.addOne(50));
	SecondClass sc = new SecondClass();
	System.out.println(sc.addTwo(50));
	}
	public static void methodNoInputNoReturn() {
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int x) {
		x-= 5;
		System.out.println("Inside the method one input no return");
		System.out.println(x);
	}
	public static void methodTwoInputNoReturn(int x, double y) {
		System.out.println(x + " " + y);
	}
	public static int methodNoInputReturnInt() {
		return 5;
	}
	public static double sumSquareRoot(int x, int y) {
		int z = x+y;
		return Math.sqrt(z);
	}
}